package cymru.prv.dictionary.api

import cymru.prv.dictionary.breton.BretonDictionary
import cymru.prv.dictionary.breton.BretonFemNumberToText
import cymru.prv.dictionary.breton.BretonMascNumberToText
import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.common.DictionaryList
import cymru.prv.dictionary.common.WordType
import cymru.prv.dictionary.common.json.Json
import cymru.prv.dictionary.common.json.JsonSerializable
import cymru.prv.dictionary.cornish.CornishDictionary
import cymru.prv.dictionary.french.FrenchDictionary
import cymru.prv.dictionary.gaelic.GaelicDictionary
import cymru.prv.dictionary.german.GermanDictionary
import cymru.prv.dictionary.irish.IrishDictionary
import cymru.prv.dictionary.welsh.WelshDictionary
import cymru.prv.dictionary.nynorsk.NynorskDictionary
import cymru.prv.dictionary.spanish.SpanishDictionary
import org.http4k.core.*
import org.http4k.core.Status.Companion.OK
import org.http4k.lens.*
import org.http4k.routing.bind
import org.http4k.routing.path
import org.http4k.routing.routes
import org.http4k.server.SunHttp
import org.http4k.server.asServer
import org.json.JSONObject
import java.io.File
import java.nio.file.Files
import java.nio.file.Path


/**
 * Creates a class that represents the API server as a whole
 * Sets up a HttpHandler for all of the end-points.
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
class OpenCelticApiServer(private val list: DictionaryList) {

    val server: HttpHandler = routes(
        "/api/dictionary" bind Method.GET to { request: Request -> general(request) },
        "/api/dictionary/ambiguous" bind Method.GET to { request: Request -> ambiguousWord(request) },
        "/api/dictionary/generate" bind Method.GET to { request: Request -> testJson(request) },
        "/api/dictionary/idlist" bind Method.GET to { request: Request -> wordIndexList(request) },
        "/api/dictionary/number" bind Method.GET to { request: Request -> numbers(request) },
        "/api/dictionary/translate" bind Method.GET to { request: Request -> translations(request) },
        "/api/dictionary/info" bind Method.GET to { information() },
        "/api/dictionary/download/{file}" bind Method.GET to { downloadFile(it) }
    )


    /**
     * The general API end-point that fetches words
     * from the dictionary.
     */
    private fun general(request: Request): Response {
        val lang = request.query("lang") ?: "cy"
        val id = Query.long().optional("id").extract(request)
        val word = Query.string().optional("word").extract(request)
        val typeString = Query.optional("type").extract(request)
        val search = Query.boolean().optional("search").extract(request)
        val type: WordType? = WordType.values().find { it.name == typeString }

        val dict = list.getDictionary(lang)
            ?: return failure("Unknown language code '$lang'")

        return when {
            typeString != null && type == null -> failure("Unknown type '$typeString'")
            id != null -> success(listOf(dict.getWordById(id)))
            word == null -> failure("Missing fields")
            search ?: false -> success(dict.getLemmas(word))
            type != null -> success(dict.getWords(word, type))
            else -> success(dict.getWords(word))
        }
    }


    /**
     * Fetches the translations for the given
     * English word.
     */
    private fun translations(request: Request): Response {
        return when (val word = request.query("word")) {
            null -> failure("Missing field 'word'")
            else -> getResponseWithHeaders()
                .body(
                    JSONObject().put("success", true)
                        .put("word", word)
                        .put("results", Json.toJsonArray(list.getTranslationsByEnglishWord(word)))
                        .toString()
                )
        }
    }


    /**
     * Export information about the current dictionaries loaded by the API.
     */
    private fun information(): Response {

        val dictionariesObj = JSONObject(list.dictionaries.entries
            .associate { it.key to it.value.compileInformationToJson() }
        )

        val obj = JSONObject()
            .put("numberOfDictionaries", list.dictionaries.size)
            .put("dictionaries", dictionariesObj)

        return getResponseWithHeaders()
            .body(obj.toString())
    }

    /**
     * Takes a number and translates it
     * into different numbers
     */
    private fun numbers(request: Request): Response {
        val num = request.query("number") ?: return failure("Missing field 'number'")
        return when (val number = num.toLongOrNull()) {
            null -> failure("Could not parse '$num' to a long")
            else -> getResponseWithHeaders()
                .body(
                    JSONObject()
                        .put("number", num)
                        .put("success", true)
                        .put("br_masc", BretonMascNumberToText().convertToText(number))
                        .put("br_fem", BretonFemNumberToText().convertToText(number))
                        .toString()
                )
        }
    }


    /**
     * Fetches all of the words within the range specified for the language
     * and returns it as a JSON object where the index is the key and the
     * normal form is the value
     */
    private fun wordIndexList(request: Request): Response {
        val lang = request.query("lang") ?: return failure("Missing field 'lang'")
        val min = Query.long().optional("min").extract(request) ?: return failure("Missing field 'min'")
        var max = Query.long().optional("max").extract(request) ?: min + 1000

        if (max - min >= 1000)
            max = min + 1000

        return when {
            !list.hasDictionary(lang) -> failure("Unknown language code '$lang'")
            else -> {
                val results = JSONObject()
                for (i in min..max) {
                    val word = list.getDictionary(lang)
                        .getWordById(i)
                    if (word != null)
                        results.put("$i", word.normalForm)
                }
                getResponseWithHeaders()
                    .body(
                        JSONObject()
                            .put("success", true)
                            .put("min", min)
                            .put("max", max)
                            .put("results", results)
                            .toString()
                    )
            }
        }
    }


    /**
     * Fetches the language, type, and json
     * from the requests and generates a word
     * based on this information.
     *
     * It is returned in the same was general
     * but there will only every be 1 word
     */
    private fun testJson(request: Request): Response {
        val jsonString = request.query("json") ?: return failure("Missing field 'json'")
        val lang = request.query("lang") ?: return failure("Missing field 'lang'")
        val typeString = request.query("type") ?: return failure("Missing field 'type'")
        val type: WordType? = WordType.values().find { it.name == typeString }

        val json = try {
            JSONObject(jsonString)
        } catch (e: Exception) {
            return failure("Could not parse JSON to JSON object")
        }

        return when {
            !list.hasDictionary(lang) -> failure("Unknown language code '$lang'")
            type == null -> failure("Unknown type '$typeString'")
            else -> return try {
                success(listOf(list.getDictionary(lang).generateWord(json, type)))
            } catch (e: Exception) {
                failure(e.message?.split("\n")?.get(0) ?: "Unknown API side error occurred")
            }
        }

    }


    /**
     * Fetches all the ambiguous terms for the
     * given word if it exists.
     */
    private fun ambiguousWord(request: Request): Response {
        val word = request.query("word") ?: return failure("Missing field 'word'")
        val units = list.getAmbiguousWord(word) ?: return success(listOf())
        return success(listOf(units))
    }


    /**
     * Used to fetch dictionaries that have been exported to a static
     * file format. Returns 404 if the file is not found
     */
    private fun downloadFile(request: Request): Response {
        val path = request.path("file")
            ?: return getResponseWithHeaders(Status.NOT_FOUND)
        val file = File("out/dict/${path}")
        if(!file.exists())
            return getResponseWithHeaders(Status.NOT_FOUND)
        return getResponseWithHeaders()
            .body(file.inputStream())
    }


    fun exportDictionariesToJson(){
        list.dictionaries.forEach { (lang, dict) ->
            println("Exporting $lang")
            val file = File("out/dict/$lang.json")
            file.parentFile.mkdirs()
            file.writeText(dict.exportDictionaryToJson().toString())
        }
    }
}


/**
 * Runs the API server and sets the server up
 * to listen to port 13999.
 */
fun main() {
    println("Starting up webserver")
    val api = OpenCelticApiServer(setupDictionary())
    api.server.asServer(SunHttp(port = 13999)).start()
    println("Ready")
    println("Exporting dictionaries to JSON")
    api.exportDictionariesToJson()
}


fun setupDictionary(): DictionaryList {
    println("Loading dictionaries")
    val list = DictionaryList()
    addDictionary(list, ::BretonDictionary)
    addDictionary(list, ::WelshDictionary)
    addDictionary(list, ::FrenchDictionary)
    addDictionary(list, ::GermanDictionary)
    addDictionary(list, ::SpanishDictionary)
    addDictionary(list, ::IrishDictionary)
    addDictionary(list, ::GaelicDictionary)
    addDictionary(list, ::CornishDictionary)
    addDictionary(list, ::NynorskDictionary)
    return list
}

/**
 * Adds the dictionary to the dictionary list
 * and prints a message once it has done that
 */
fun addDictionary(list: DictionaryList, constr: (list: DictionaryList) -> Dictionary) {
    val dict = constr.invoke(list)
    println("Imported dictionary '${dict.languageCode}' with ${dict.numberOfWords} words")
}


/**
 * Generates the basic JSON structure for a successful
 * API call. Success should be set to true and the
 * objects returned from the API should be in a field
 * called 'results'
 *
 * @param data The list of words from the API
 */
fun success(data: List<JsonSerializable>): Response {
    val resp = getResponseWithHeaders()
    val obj = JSONObject()
        .put("success", true)
        .put("results", Json.toJsonArray(data))
    return resp.body(obj.toString())
}


/**
 * Creates the response for a failed API call.
 * Sends back a JSON object with the success flag
 * set to false and an error message.
 *
 * @param message The error message to return
 */
fun failure(message: String): Response {
    val resp = getResponseWithHeaders()
    val obj = JSONObject()
        .put("success", false)
        .put("message", message)
    return resp.body(obj.toString())
}


/**
 * Sets up the basic response with common headers
 * and stuff like that
 * @param status The status code to return
 */
fun getResponseWithHeaders(status: Status = OK): Response {
    return Response(status)
        .header("Content-Type", "application/json; charset=utf-8")
        .header("Access-Control-Allow-Origin", "*")
}