package cymru.prv.dictionary.api

import cymru.prv.dictionary.common.Dictionary
import cymru.prv.dictionary.common.DictionaryList
import org.http4k.core.Method
import org.http4k.core.Request
import org.http4k.core.Response
import org.http4k.core.Status
import org.json.JSONObject
import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Nested
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.TestInstance
import kotlin.test.assertEquals
import kotlin.test.assertFalse
import kotlin.test.assertTrue

/**
 * A simple test class to test all of
 * the end-points of the API server to ensure
 * that every thing is working correctly
 *
 * @author Preben Vangberg
 * @since 1.0.0
 */
@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TestOpenCelticApiServer {

    val list = setupDictionary()
    val api = OpenCelticApiServer(list)

    @Nested
    inner class TestGeneralEndPoint {

        @Test
        fun `should be able to search for word`(){
            val response = api.server(Request(Method.GET, "/api/dictionary?word=byddwn&search=true")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertTrue(obj.getBoolean("success"))
            assertEquals("bod", obj.getJSONArray("results").getJSONObject(0).getString("normalForm"))
        }

        @Test
        fun `words with ' should work`(){
            val response = api.server(Request(Method.GET, "/api/dictionary?word=abcde&type=noun")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertTrue(obj.getBoolean("success"))
            val word = obj.getJSONArray("results").getJSONObject(0)
            assertEquals("abcde", word.get("normalForm"))
            assertFalse(word.getBoolean("confirmed"))
        }

        @Test
        fun `success should be false if no parameters was passed`(){
            val response = api.server(Request(Method.GET, "/api/dictionary")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertFalse(obj.has("results"))
            assertTrue(obj.has("message"))
        }

        @Test
        fun `should return an error if language does not exist`(){
            val response = api.server(Request(Method.GET, "/api/dictionary?lang=abc")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertFalse(obj.has("results"))
            assertEquals("Unknown language code 'abc'", obj.getString("message"))
        }

        @Test
        fun `should be able to fetch with ID`(){
            val response = api.server(Request(Method.GET, "/api/dictionary?id=0")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertTrue(obj.getBoolean("success"))
            assertEquals("bod", obj.getJSONArray("results").getJSONObject(0).getString("normalForm"))
        }

        @Test
        fun `should be able to fetch with word without type`(){
            val response = api.server(Request(Method.GET, "/api/dictionary?word=gweld")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertTrue(obj.getBoolean("success"))
            assertEquals("gweld", obj.getJSONArray("results").getJSONObject(0).getString("normalForm"))
        }

        @Test
        fun `should return an error if type does not exist`(){
            val response = api.server(Request(Method.GET, "/api/dictionary?type=test")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertEquals("Unknown type 'test'", obj.getString("message"))
        }

        @Test
        fun `should generate word if type is given`(){
            val response = api.server(Request(Method.GET, "/api/dictionary?word=abcde&type=noun")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertTrue(obj.getBoolean("success"))
            val word = obj.getJSONArray("results").getJSONObject(0)
            assertEquals("abcde", word.get("normalForm"))
            assertFalse(word.getBoolean("confirmed"))
        }
    }

    @Nested
    inner class TestTranslationsEndPoint {

        @Test
        fun `should return an error if word is not given for translation`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/translate")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertEquals("Missing field 'word'", obj.getString("message"))
        }

        @Test
        fun `should return data if translation exist`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/translate?word=green")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertTrue(obj.getBoolean("success"))
            assertEquals("green", obj.getString("word"))
            assertTrue(obj.has("results"))
            val results = obj.getJSONArray("results")
            assertTrue(results.length() > 0)
            assertTrue(results.getJSONObject(0).length() > 0)
        }

        @Test
        fun `should at least one data if ambiguous translation exist`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/translate?word=to%20know")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertTrue(obj.getBoolean("success"))
            assertEquals("to know", obj.getString("word"))
            assertTrue(obj.has("results"))
            val results = obj.getJSONArray("results")
            assertTrue(results.length() > 0)
            assertTrue(results.getJSONObject(0).length() > 0)
        }

    }

    @Nested
    inner class TestNumberEndPoint {

        @Test
        fun `should return error if number is missing`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/number")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertEquals("Missing field 'number'", obj.getString("message"))
        }

        @Test
        fun `should return an error if the number can not be read`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/number?number=100a")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertEquals("Could not parse '100a' to a long", obj.getString("message"))
        }

        @Test
        fun `should return the converted numbers if number is valid`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/number?number=250")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertTrue(obj.getBoolean("success"))
            assertTrue(obj.has("br_masc"))
        }

    }

    @Nested
    inner class TestWordIndexEndPoint {

        @Test
        fun `should return error if lang is missing`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/idlist?min=50")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertEquals("Missing field 'lang'", obj.getString("message"))
        }

        @Test
        fun `should return error if min is missing`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/idlist?lang=cy")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertEquals("Missing field 'min'", obj.getString("message"))
        }

        @Test
        fun `should return error if lang does not exist`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/idlist?lang=test&min=0")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertEquals("Unknown language code 'test'", obj.getString("message"))
        }

        @Test
        fun `should cap returns at max`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/idlist?lang=cy&min=0&max=1")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertTrue(obj.getBoolean("success"))
            val results = obj.getJSONObject("results")
            assertEquals(2, results.length())
            assertTrue(results.has("0"))
            assertTrue(results.has("1"))
            assertFalse(results.has("2"))
        }

        @Test
        fun `should cap max at 1000 above min`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/idlist?lang=cy&min=0&max=2000")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertTrue(obj.getBoolean("success"))
            val results = obj.getJSONObject("results")
            assertTrue(results.has("0"))
            assertTrue(results.has("1000"))
            assertFalse(results.has("1001"))
        }

    }

    @Nested
    inner class TestJsonGeneratorEndPoint {

        @Test
        fun `should return an error if lang is missing`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/generate?type=noun&json={}")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertEquals("Missing field 'lang'", obj.getString("message"))
        }

        @Test
        fun `should return an error if type is missing`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/generate?lang=cy&json={}")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertEquals("Missing field 'type'", obj.getString("message"))
        }

        @Test
        fun `should return an error if json is missing`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/generate?lang=cy&type=noun")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertEquals("Missing field 'json'", obj.getString("message"))
        }

        @Test
        fun `should return an error if type does not exist`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/generate?type=test&lang=cy&json={}")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertEquals("Unknown type 'test'", obj.getString("message"))
        }

        @Test
        fun `should return an error if json cannot be converted`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/generate?type=noun&lang=cy&json={asd}")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertEquals("Could not parse JSON to JSON object", obj.getString("message"))
        }

        @Test
        fun `should return error if lang does not exist`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/generate?type=noun&lang=test&json={}")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertEquals("Unknown language code 'test'", obj.getString("message"))
        }

        @Test
        fun `should return error if data is not valid`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/generate?type=noun&lang=cy&json={}")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertEquals("Normal form cannot be empty.", obj.getString("message"))
        }

        @Test
        fun `should be able to generate words successfully`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/generate?type=noun&lang=cy&json={\"normalForm\":\"test\"}")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertTrue(obj.getBoolean("success"))
            assertEquals(1, obj.getJSONArray("results").length())
            val word = obj.getJSONArray("results").getJSONObject(0)
            assertFalse(word.getBoolean("confirmed"))
            assertEquals("test", word.getString("normalForm"))
        }

        @Test
        fun `should be able to generate words successfully with special characters`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/generate?type=noun&lang=br&json={\"normalForm\":\"bezañ\"}")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertTrue(obj.getBoolean("success"))
            val word = obj.getJSONArray("results").getJSONObject(0)
            assertFalse(word.getBoolean("confirmed"))
            assertEquals("bezañ", word.getString("normalForm"))
        }

    }

    @Nested
    inner class TestAmbiguousEndPoint {

        @Test
        fun `should return an error if word is missing`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/ambiguous")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertFalse(obj.getBoolean("success"))
            assertEquals("Missing field 'word'", obj.getString("message"))
        }

        @Test
        fun `should return an empty list if word does not exist`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/ambiguous?word=qxz")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertTrue(obj.getBoolean("success"))
            assertTrue(obj.getJSONArray("results").isEmpty)
        }

        @Test
        fun `should return a list with results if word does exist`(){
            val response = api.server(Request(Method.GET, "/api/dictionary/ambiguous?word=to know")).expectOK()
            val obj = JSONObject(response.bodyString())
            assertTrue(obj.getBoolean("success"))
            assertTrue(!obj.getJSONArray("results").isEmpty)
        }

    }

    @Nested
    inner class TestInformationEndPoint {

        @Test
        fun `end-point should export JSON document with all dictionaries`(){
            val obj = api.server(Request(Method.GET, "/api/dictionary/info"))
                .expectOK()
                .toJson()
            assertTrue(obj.has("numberOfDictionaries"))
            assertTrue(obj.has("dictionaries"))
            assertEquals(
                obj.getInt("numberOfDictionaries"),
                obj.getJSONObject("dictionaries").length()
            )
        }

    }

    @Nested
    inner class TestExportEndpoint {

        @Test
        fun `test that api is able to return file`(){
            api.exportDictionariesToJson()
            val obj = api.server(Request(Method.GET, "/api/dictionary/download/cy.json"))
                .expectOK()
                .toJson()
            Assertions.assertEquals("cy", obj.getString("lang"))
        }

        @Test
        fun `should return 404 if file is missing`(){
            val resp = api.server(Request(Method.GET, "/api/dictionary/download/test.test"))
            Assertions.assertEquals(Status.NOT_FOUND, resp.status)
        }


    }
}


private fun Response.expectOK(): Response {
    assertEquals(this.status, Status.OK)
    return this;
}

private fun Response.toJson(): JSONObject {
    return JSONObject(this.bodyString())
}
